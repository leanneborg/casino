# Assumptions

While developing the Casino, the following assumptions were made:
 
 * Even though some games, specifically the live casino ones, are not available for demo, these were still listed since one would still want to promote such games.
 * It was assumed that games would not be removed from the system without a maintenance window, and as a result would require the user to request the data from the server. This restriction can be improved by introducing expiration on the cached data.
