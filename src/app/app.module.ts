import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { GameListingComponent } from './game-listing/game-listing.component';
import { CategoriesComponent } from './categories/categories.component';
import { GameDetailsComponent } from './game-details/game-details.component';

import { KeyNgFor, SafePipe } from './app-pipes';
import { DataService } from './data.service';
import { SearchComponent } from './search/search.component';

import { RouterModule, Routes } from '@angular/router';
import { MessageService } from './message.service';

const appRoutes: Routes = [
  { path: 'game/:slug', component: GameDetailsComponent },
  { path: 'category/:slug', component: GameListingComponent },
  { path: '**', component: GameListingComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    KeyNgFor,
    SafePipe,
    GameListingComponent,
    CategoriesComponent,
    GameDetailsComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
