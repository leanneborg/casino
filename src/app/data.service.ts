import {Injectable, OnInit} from '@angular/core';
import {  HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DataService {

  private baseUrl = 'https://api-live.voodoodreams.com';
  private games: Map<number, any>;
  private categories: Map<string, any>;
  private gameDetails: Map<string, any>;

  constructor(private http: HttpClient) {}

  getGames(onComplete?, category?: string) {

    if (this.games) {
      return this.filterGames(category);
    }

    this.performGet('/v1/games', data => {
      this.games = data['games'];
    }, onComplete);
  }

  private filterGames(category?: string) {

    if (!category || this.categories.get(category)['also-show-others-in-group']) {
      return this.games;
    }

    const games: Map<number, any> = new Map();
    for (let gameId of this.categories.get(category)['game-ids']) {
      games[gameId] = this.games[gameId];
    }

    return games;
  }

  getCategories(onComplete?) {

    if (this.categories) {
      // Get the values of the map
      let categoriesList: any[] = Array.from(this.categories.values());
      let otherGamesCat: any[];

      // Seperate the list of categories to extract the other games category in a seperate list
      for (let cat of categoriesList) {
        if(cat['slug'].indexOf('other') > -1) {
          otherGamesCat = categoriesList.splice(categoriesList.indexOf(cat), 1);
          break;
        }
      }

      // Sort the list of categories not containing the other games category
      categoriesList = categoriesList.sort((a, b) => {
          return a['slug'].localeCompare(b['slug']);
      });

      // Add the other games category at the end of the list
      if(otherGamesCat.length > 0) {
        categoriesList.push(otherGamesCat[0]);
      }

      return categoriesList;
    }

    this.performGet('/v1/games/categories', data => {
      this.categories = new Map(data['categories'].map((i) => [i['slug'], i]));
    }, onComplete);
  }


  getGame(slug: string, onComplete?) {

    if (this.gameDetails && this.gameDetails.get(slug)) {
      return this.gameDetails.get(slug);
    }

    this.performGet('/v1/games/' + slug, data => {
      if (!this.gameDetails) {
        this.gameDetails = new Map();
      }
      this.gameDetails.set(slug, data);
    }, onComplete);
  }


  private performGet(url: string, onSuccess, onComplete) {

    // Make an HTTP request to retrieve all games
    return this.http.get(this.baseUrl + url, {
      headers: new HttpHeaders().set('secure', 'false')}
      ).subscribe(onSuccess, err => {
      console.log('Failed to perform request ${url} with HTTP status ${err.status}');
    }, onComplete);
  }

  private orderCategories(categories) {

  }
}
