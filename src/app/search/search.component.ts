import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private query: string = null;

  constructor(private messageService: MessageService) {}

  ngOnInit() {
  }

  onKey(event: any) {
    this.query = event.target.value;
    this.messageService.sendMessage(this.query);
  }

  clearQuery() {
    this.query = '';
    this.messageService.sendMessage(this.query);
  }
}
