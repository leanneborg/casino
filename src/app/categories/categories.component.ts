import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  private categories: any[];

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.categories = this.dataService.getCategories(() => {
      this.categories = this.dataService.getCategories();
    });
  }
}
