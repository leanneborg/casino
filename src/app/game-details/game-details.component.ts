import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit, OnDestroy {

  private gameSlug: string;
  private game: any;
  private sub: Subscription;

  constructor(private dataService: DataService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.gameSlug = params['slug'];
    });
    this.game = this.dataService.getGame(this.gameSlug, () => {
      this.game = this.dataService.getGame(this.gameSlug);
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
