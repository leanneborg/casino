import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-game-listing',
  templateUrl: './game-listing.component.html',
  styleUrls: ['./game-listing.component.css']
})
export class GameListingComponent implements OnInit, OnDestroy {

  private games: Map<number, any>;
  private filteredGames: Map<number, any>;
  private category: string;
  private sub: Subscription;
  private routeSub: Subscription;
  private query: string = null;

  constructor(private dataService: DataService, private messageService: MessageService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.messageService.getMessage().subscribe(message => {
      if (message) {
        this.query = message.text.toLowerCase();
        this.filterGames();
      }
      console.log(this.query);
    });
    this.routeSub = this.route.params.subscribe(params => {
      this.category = params['slug'];
      this.loadGames();
    });
    this.loadGames();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private loadGames() {
    this.games = this.dataService.getGames(() => {
      this.games = this.dataService.getGames(this.category);
      this.filterGames();
    }, this.category);
    this.filterGames();
  }


  private filterGames() {
    if (!this.query) {
      this.filteredGames = this.games;
      return;
    }

    this.filteredGames = new Map();
    const lowercaseQuery: string = this.query.toLowerCase();

    for (let game of Object.values(this.games)) {
      if(game['title'].toLowerCase().indexOf(lowercaseQuery) > -1) {
        this.filteredGames[game['game-id']] = game;
      }
    }
  }
}
