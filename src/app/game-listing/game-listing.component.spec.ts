import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameListingComponent } from './game-listing.component';
import { KeyNgFor } from '../app-pipes';
import { RouterTestingModule } from '@angular/router/testing';
import { DataService } from '../data.service';
import { MessageService } from '../message.service';
import { HttpClientModule } from '@angular/common/http';

describe('GameListingComponent', () => {
  let component: GameListingComponent;
  let fixture: ComponentFixture<GameListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameListingComponent, KeyNgFor ],
      imports: [ RouterTestingModule, HttpClientModule ],
      providers: [ DataService, MessageService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
