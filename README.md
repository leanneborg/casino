# Casino

This project is an online casino which allows the user to run games in demo mode. It has been generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Prerequisites

The following prerequisites need to be satisfied before being able to build and run the project:

* NPM
* Angular CLI

## Build

The project can be built using `ng build` and tests can be run using `ng test`.

## Development server

Run `ng serve` for a dev server and navigate to `http://localhost:4200/`.
